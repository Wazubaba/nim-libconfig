# Package

version       = "0.9.0"
author        = "Wazubaba"
description   = "Raw wrapper around libconfig"
license       = "LGPLv3"
installExt    = @["nim"]

# Dependencies

requires "nim >= 0.20.0"

