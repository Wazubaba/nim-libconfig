import unittest
import libconfig

var
  cfg: config_t
  setting: ptr config_setting_t
  sval: cstring
  ival: cint
  bval: cint
  fval: cdouble
  lval: clonglong

config_init(cfg.addr)
echo $config_read_file(cfg.addr, "test.cfg")

echo "Config tab width is ", config_get_tab_width(cfg.addr)

echo $config_lookup_string(cfg.addr, "application.window.title", sval.addr)
echo "Got value: ", sval

echo $config_lookup_int(cfg.addr, "application.a", ival.addr)
echo "Got value: ", ival

echo $config_lookup_int64(cfg.addr, "misc.bigint", lval.addr)
echo "Got value: ", lval

echo $config_lookup_float(cfg.addr, "misc.pi", fval.addr)
echo "Got value: ", fval

echo $config_lookup_bool(cfg.addr, "application.group1.flag", bval.addr)
echo "Got value: ", if bval == 1: "true" else: "false"

proc destructortest(data: pointer) =
  echo "Destructor called! :D"

setting = config_lookup(cfg.addr, "application.b")
assert setting != nil

let sival = config_setting_get_int(setting)
assert sival == 6

assert config_setting_set_int(setting, 8) != CONFIG_FALSE

assert config_write_file(cfg.addr, "OUT.conf") != CONFIG_FALSE
config_set_destructor(cfg.addr, destructortest)
config_destroy(cfg.addr)

