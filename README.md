# libconfig-nim
This is currently a 1:1 raw wrapper for libconfig. I might and probably even
will be making a higher-level 'nimified' binding for it later, but this at
least will permit one to use this fantastic library.

You need to have the libconfig library installed and in your system link
paths.

Please visit [hyperrealm][1] for more information. You can visit [docs][2] to
find the documentation for the base library.

[1]: https://hyperrealm.github.io/libconfig
[2]: https://hyperrealm.github.io/libconfig/libconfig_manual.html#The-C-API

