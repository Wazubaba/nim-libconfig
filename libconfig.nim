## This module serves as a wrapper around libconfig

when defined(linux):
  from os import findExe
  const LIBNAME = "libconfig.so"
  # This somehow prevents a memory leak according to valgrind. God only knows why...
  if findExe("pkg-config") != "":
    # Do it correctly
    {.passL: gorge("pkg-config --libs libconfig").}
  else:
    # Take a shot in the dark
    {.passL: "-lconfig".}
elif defined(macosx):
  const LIBNAME = "libconfig.dynlib"
elif defined(windows):
  const LIBNAME = "libconfig.dll"
elif defined(posix):
  const LIBNAME = "libconfig.so"

# TODO: make this a bit smarter perhaps?
#
#{.pragma: lcinject, header: "<libconfig.h>", importc.}

const
  # Config bool types
  CONFIG_TRUE*: cint = 1
  CONFIG_FALSE*: cint = 0

  # Config options
  CONFIG_OPTION_AUTOCONVERT*: cint = 0x01
  CONFIG_OPTION_SEMICOLON_SEPARATORS*: cint = 0x02
  CONFIG_OPTION_COLON_ASSIGNMENT_FOR_GROUPS*: cint = 0x04
  CONFIG_OPTION_COLON_ASSIGNMENT_FOR_NON_GROUPS*: cint = 0x08
  CONFIG_OPTION_OPEN_BRACE_ON_SEPARATE_LINE*: cint = 0x10
  CONFIG_OPTION_ALLOW_SCIENTIFIC_NOTATION*: cint = 0x20
  CONFIG_OPTION_FSYNC*: cint = 0x40

  # Config version infos
  LIBCONFIG_VER_MAJOR*: cint = 1
  LIBCONFIG_VER_MINOR*: cint = 7
  LIBCONFIG_VER_REVISION*: cint = 0

  # Config types
  CONFIG_TYPE_NONE*: cint = 0
  CONFIG_TYPE_GROUP*: cint = 1
  CONFIG_TYPE_INT*: cint = 2
  CONFIG_TYPE_INT64*: cint = 3
  CONFIG_TYPE_FLOAT*: cint = 4
  CONFIG_TYPE_STRING*: cint = 5
  CONFIG_TYPE_BOOL*: cint = 6
  CONFIG_TYPE_ARRAY*: cint = 7
  CONFIG_TYPE_LIST*: cint = 8

  # Config format types
  CONFIG_FORMAT_DEFAULT*: cint = 0
  CONFIG_FORMAT_HEX*: cint = 1

  # Config error types
  CONFIG_ERR_NONE* = 0
  CONFIG_ERR_FILE_IO* = 1
  CONFIG_ERR_PARSE* = 2

type
  # AFAIK these are the wrong way to do this
  # config_t* {.lcinject.} = object
  # config_setting_t* {.lcinject.} = object

  config_include_fn_t* = proc(config: ptr config_t, include_dir: cstring, path: cstring, error: ptr cstring): ptr cstring
  config_destructor_fn* = proc(data: pointer)

  config_list_t = object
    length: cuint
    elements: UncheckedArray[ptr config_setting_t]

  config_value_t {.union.} = object
    ival: cint
    llval: clonglong
    fval: cdouble
    sval: cstring
    list: ptr config_list_t

  config_setting_t* = object
    name: cstring
    `type`: cshort
    format: cshort
    value: config_value_t
    parent: ptr config_setting_t
    config: ptr config_t
    hook: pointer
    line: cuint
    file: cstring


  config_t* = object
    root: ptr config_setting_t
    destructor: config_destructor_fn
    options: cint
    tab_width: cushort
    float_precision: cushort
    default_format: cushort
    include_fn: config_include_fn_t
    error_text: cstring
    error_file: cstring
    error_line: cint
    error_type: cint
    filenames: ptr cstring
    hook: pointer

    

proc config_init*(config: ptr config_t) {.dynlib: LIBNAME, importc.}
proc config_destroy*(config: ptr config_t) {.dynlib: LIBNAME, importc.}
proc config_clear*(config: ptr config_t) {.dynlib: LIBNAME, importc.}

proc config_read*(config: ptr config_t, stream: File): cint {.dynlib: LIBNAME, importc.}
proc config_read_file* (config: ptr config_t, filename: cstring): cint {.dynlib: LIBNAME, importc.}
proc config_read_string*(config: ptr config_t, str: cstring): cint {.dynlib: LIBNAME, importc.}

proc config_write*(config: ptr config_t, stream: File): cint {.dynlib: LIBNAME, importc.}
proc config_write_file*(config: ptr config_t, filename: cstring): cint {.dynlib: LIBNAME, importc.}

proc config_error_text*(config: ptr config_t): cstring {.dynlib: LIBNAME, importc.}
proc config_error_file*(config: ptr config_t): cstring {.dynlib: LIBNAME, importc.}
proc config_error_line*(config: ptr config_t): cint {.dynlib: LIBNAME, importc.}
proc config_error_type*(config: ptr config_t): cint {.dynlib: LIBNAME, importc.}

proc config_set_include_dir*(config: ptr config_t, include_dir: cstring) {.dynlib: LIBNAME, importc.}
proc config_get_include_dir*(config: ptr config_t): cstring {.dynlib: LIBNAME, importc.}
proc config_set_include_func*(incfunc: config_include_fn_t) {.dynlib: LIBNAME, importc.} ## WARNING: IDK IF THIS WILL EVEN WORK

proc config_get_float_precision*(config: ptr config_t): cushort {.dynlib: LIBNAME, importc.}
proc config_set_float_precision*(config: ptr config_t, digits: cushort) {.dynlib: LIBNAME, importc.}

proc config_get_options*(config: ptr config_t): cint {.dynlib: LIBNAME, importc.}
proc config_set_options*(config: ptr config_t, options: cint) {.dynlib: LIBNAME, importc.}

proc config_get_option*(config: ptr config_t, option: cint): cint {.dynlib: LIBNAME, importc.}
proc config_set_option*(config: ptr config_t, option: cint, flag: cint) {.dynlib: LIBNAME, importc.}

proc config_get_auto_convert*(config: ptr config_t): cint {.dynlib: LIBNAME, importc.}
proc config_set_auto_convert*(config: ptr config_t, flag: cint) {.dynlib: LIBNAME, importc.}

proc config_get_default_format*(config: ptr config_t): cshort {.dynlib: LIBNAME, importc.}
proc config_set_default_format*(config: ptr config_t, format: cshort) {.dynlib: LIBNAME, importc.}

proc config_get_tab_width*(config: ptr config_t): cushort {.dynlib: LIBNAME, importc.}
proc config_set_tab_width*(config: ptr config_t, width: cushort) {.dynlib: LIBNAME, importc.}

proc config_lookup_int*(config: ptr config_t, path: cstring, value: ptr cint): cint {.dynlib: LIBNAME, importc.}
proc config_lookup_int64*(config: ptr config_t, path: cstring, value: ptr clonglong): cint {.dynlib: LIBNAME, importc.}
proc config_lookup_float*(config: ptr config_t, path: cstring, value: ptr cdouble): cint {.dynlib: LIBNAME, importc.}
proc config_lookup_bool*(config: ptr config_t, path: cstring, value: ptr cint): cint {.dynlib: LIBNAME, importc.}
proc config_lookup_string*(config: ptr config_t, path: cstring, value: ptr cstring): cint {.dynlib: LIBNAME, importc.}

proc config_lookup*(config: ptr config_t, path: cstring): ptr config_setting_t {.dynlib: LIBNAME, importc.}
proc config_setting_lookup*(setting: ptr config_setting_t, path: cstring): ptr config_setting_t {.dynlib: LIBNAME, importc.}

proc config_setting_get_int*(setting: ptr config_setting_t): cint {.dynlib: LIBNAME, importc.}
proc config_setting_get_int64*(setting: ptr config_setting_t): clonglong {.dynlib: LIBNAME, importc.}
proc config_setting_get_float*(setting: ptr config_setting_t): cdouble {.dynlib: LIBNAME, importc.}
proc config_setting_get_bool*(setting: ptr config_setting_t): cint {.dynlib: LIBNAME, importc.}
proc config_setting_get_string*(setting: ptr config_setting_t): cstring {.dynlib: LIBNAME, importc.}

proc config_setting_set_int*(setting: ptr config_setting_t, value: cint): cint {.dynlib: LIBNAME, importc.}
proc config_setting_set_int64*(setting: ptr config_setting_t, value: clonglong): clonglong {.dynlib: LIBNAME, importc.}
proc config_setting_set_float*(setting: ptr config_setting_t, value: cdouble): cdouble {.dynlib: LIBNAME, importc.}
proc config_setting_set_bool*(setting: ptr config_setting_t, value: cint): cint {.dynlib: LIBNAME, importc.}
proc config_setting_set_string*(setting: ptr config_setting_t, value: cstring): cstring {.dynlib: LIBNAME, importc.}

proc config_setting_lookup_int*(setting: ptr config_setting_t, name: cstring, value: ptr cint): cint {.dynlib: LIBNAME, importc.}
proc config_setting_lookup_int64*(setting: ptr config_setting_t, name: cstring, value: ptr clonglong): clonglong {.dynlib: LIBNAME, importc.}
proc config_setting_lookup_float*(setting: ptr config_setting_t, name: cstring, value: ptr cdouble): cdouble {.dynlib: LIBNAME, importc.}
proc config_setting_lookup_bool*(setting: ptr config_setting_t, name: cstring, value: ptr cint): cint {.dynlib: LIBNAME, importc.}
proc config_setting_lookup_string*(setting: ptr config_setting_t, name: cstring, value: ptr cstring): cstring {.dynlib: LIBNAME, importc.}

proc config_setting_get_format*(setting: ptr config_setting_t): cshort {.dynlib: LIBNAME, importc.}
proc config_setting_set_format*(setting: ptr config_setting_t, format: cshort) {.dynlib: LIBNAME, importc.}

proc config_setting_get_member*(setting: ptr config_setting_t, name: cstring): ptr config_setting_t {.dynlib: LIBNAME, importc.}

proc config_setting_get_elem*(setting: ptr config_setting_t, index: cuint): ptr config_setting_t {.dynlib: LIBNAME, importc.}

proc config_setting_get_int_elem*(setting: ptr config_setting_t, index: cint): cint {.dynlib: LIBNAME, importc.}
proc config_setting_get_int64_elem*(setting: ptr config_setting_t, index: cint): clonglong {.dynlib: LIBNAME, importc.}
proc config_setting_get_float_elem*(setting: ptr config_setting_t, index: cint): cdouble {.dynlib: LIBNAME, importc.}
proc config_setting_get_bool_elem*(setting: ptr config_setting_t, index: cint): cint {.dynlib: LIBNAME, importc.}
proc config_setting_get_string_elem*(setting: ptr config_setting_t, index: cint): cstring {.dynlib: LIBNAME, importc.}

proc config_setting_set_int_elem*(setting: ptr config_setting_t, index: cint, value: cint) {.dynlib: LIBNAME, importc.}
proc config_setting_set_int64_elem*(setting: ptr config_setting_t, index: cint, value: clonglong) {.dynlib: LIBNAME, importc.}
proc config_setting_set_float_elem*(setting: ptr config_setting_t, index: cint, value: cdouble) {.dynlib: LIBNAME, importc.}
proc config_setting_set_bool_elem*(setting: ptr config_setting_t, index: cint, value: cint) {.dynlib: LIBNAME, importc.}
proc config_setting_set_string_elem*(setting: ptr config_setting_t, index: cint, value: cstring) {.dynlib: LIBNAME, importc.}

proc config_setting_add*(parent: ptr config_setting_t, name: cstring, settingType: cint): ptr config_setting_t {.dynlib: LIBNAME, importc.}

proc config_setting_remove*(parent: ptr config_setting_t, name: cstring): cint {.dynlib: LIBNAME, importc.}

proc config_setting_remove_elem*(parent: ptr config_setting_t, index: cuint): cint {.dynlib: LIBNAME, importc.}

proc config_root_setting*(config: ptr config_t): ptr config_setting_t {.dynlib: LIBNAME, importc.}

proc config_setting_name*(setting: ptr config_setting_t): cstring {.dynlib: LIBNAME, importc.}

proc config_setting_parent*(setting: ptr config_setting_t): ptr config_setting_t {.dynlib: LIBNAME, importc.}

proc config_setting_is_root*(setting: ptr config_setting_t): cint {.dynlib: LIBNAME, importc.}

proc config_setting_index*(setting: ptr config_setting_t): cint {.dynlib: LIBNAME, importc.}

proc config_setting_length*(setting: ptr config_setting_t): cint {.dynlib: LIBNAME, importc.}

proc config_setting_type*(setting: ptr config_setting_t): cint {.dynlib: LIBNAME, importc.}

proc config_setting_is_group*(setting: ptr config_setting_t): cint {.dynlib: LIBNAME, importc.}
proc config_setting_is_array*(setting: ptr config_setting_t): cint {.dynlib: LIBNAME, importc.}
proc config_setting_is_list*(setting: ptr config_setting_t): cint {.dynlib: LIBNAME, importc.}

proc config_setting_is_aggregate*(setting: ptr config_setting_t): cint {.dynlib: LIBNAME, importc.}
proc config_setting_is_scalar*(setting: ptr config_setting_t): cint {.dynlib: LIBNAME, importc.}
proc config_setting_is_number*(setting: ptr config_setting_t): cint {.dynlib: LIBNAME, importc.}

proc config_setting_source_file*(setting: ptr config_setting_t): cstring {.dynlib: LIBNAME, importc.}

proc config_setting_source_line*(setting: ptr config_setting_t): cuint {.dynlib: LIBNAME, importc.}

proc config_set_hook*(config: ptr config_t, hook: pointer) {.dynlib: LIBNAME, importc.}
proc config_get_hook*(config: ptr config_t): pointer {.dynlib: LIBNAME, importc.}

proc config_setting_set_hook*(setting: ptr config_setting_t, hook: pointer) {.dynlib: LIBNAME, importc.}
proc config_setting_get_hook*(setting: ptr config_setting_t): pointer {.dynlib: LIBNAME, importc.}

proc config_set_destructor*(config: ptr config_t, destructor: config_destructor_fn) {.dynlib: LIBNAME, importc.}

